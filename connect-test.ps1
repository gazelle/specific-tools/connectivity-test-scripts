# Copyright 2023 IHE International (http://www.ihe.net)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

[cmdletbinding(  
    DefaultParameterSetName = '',  
    ConfirmImpact = 'low'  
)]

Param
    (
        [string] $csv,
        [string] $hostname,
        [int] $port,
        [switch] $udp,
        [string] $msg="connectivity-test",
        [switch] $help
    )

function show_help() {
	Write-Host "HELP:"
	Write-Host "connect-test performs a telnet call to the given host and port"
	Write-Host " -csv file (Gazelle Configuration file as CSV to iterate over) NOT IMPLEMENTED" 
	Write-Host " -h hostname (Host or IP address to target, if -csv is not used)"
	Write-Host " -p port (Port to target, required if -h is used)"
	Write-Host " -udp (Switch to UDP protocol, only if -h is used) NOT IMPLEMENTED"
	Write-Host " -msg message (Message to send during the telnet attempt, default is 'connectivity-test')"
	Write-Host " -help (display this help)"
	Write-Host "example : connect-test -csv configuration.csv -msg \"Connectivity test from my-system-name\""
	Write-Host "example : connect-test -h 127.0.0.1 -p 80 -msg \"Connectivity test from my-system-name\""
}

function check_param(){
    if ($hostname -eq "") { 
        Write-Host "Host is empty; Please retry with providing a domain name or ip address."
        Write-Host "Connect-test.ps1 -help"
        exit
    }
    if ($port -eq 0) {
        Write-Host "Port is empty; Please retry with providing a port number."
        Write-Host "Connect-test.ps1 -help"
        exit
    }
}



function use_csv(){
    if (Test-Path $csv){
        $tabCmd = Import-Csv -Path $csv
        Write-Host "Sys,Actor,Type,Host name,IP,Port,host test result,ip test result,remark"

        foreach ($currentItemName in $tabCmd) {
            $sys = $currentItemName.Sys
            $actor = $currentItemName.Actor
            $type = $currentItemName.Type
            $hostname = $currentItemName."Host name"
            $ip = $currentItemName.IP
            $port = $currentItemName.Port

            if (Test-NetConnection -ComputerName $ip -Port $port -InformationLevel Quiet -InformationVariable $msg){
                $currentItemName | Add-Member -NotePropertyName "ip test result" -NotePropertyValue "PASSED"
            }else{
                $currentItemName | Add-Member -NotePropertyName "ip test result" -NotePropertyValue "FAILED"
            }
            if (Test-NetConnection -ComputerName $hostname -Port $port -InformationLevel Quiet -InformationVariable $msg){
                $currentItemName | Add-Member -NotePropertyName "host test result" -NotePropertyValue "PASSED"
            }else{
                $currentItemName | Add-Member -NotePropertyName "host test result" -NotePropertyValue "FAILED"
            }
            if ($($currentItemName.'ip test result') -eq "FAILED" -and $($currentItemName.'host test result') -eq "PASSED"){
                $remark = "Connectivity tests for this system only worked using the hostname, which may be normal depending on the system configuration."
            }else {
                $remark = ""
            }

            $currentItemName | Add-Member -NotePropertyName "remark" -NotePropertyValue  $remark

            Write-host "$sys,$actor,$type,$hostname,$ip,$port,$($currentItemName.'host test result'),$($currentItemName.'ip test result'),$($currentItemName.'remark')"

        }
        $tabCmd | Export-csv -Path .\configurations_results.csv -NoTypeInformation
    }else{
        Write-Host "The specified target $csv doesn't exist"
    }
}

function test_single() {
    check_param
    if ($udp) {
        #Creating the object and opening the port
        $udpobject = new-Object system.Net.Sockets.Udpclient($port)
        $udpobject.Client.ReceiveTimeout = 2000

        #Setting-up the string message into bytes
        $bmsg = new-object system.text.asciiencoding
        $byte = $bmsg.GetBytes("$(Get-Date)")

        #Connecting to the remote host
        $udpobject.Connect($hostname,$port)

        #Sending the message
        if ($udpobject.Send($byte,$byte.length)) {
            Write-Host "true"
        }else{
           Write-host "wrong" 
        } 

        $udpobject.Close()
        
    } else {
        if (Test-NetConnection -ComputerName $hostname -Port $port -InformationLevel quiet -InformationVariable $msg){
            Write-host "TCP response from $hostname at port $port"
        }else{
            Write-host "No TCP response from $hostname at port $port"
        }
    }
}

if ($help) {
    show_help
} elseif ($csv -ne "") {
    use_csv
} else {
    test_single
}
