# Connectivity Test Scripts

Scripts to facilitate connectivity tests during an online test event.

The script `connect-test.sh` can be used to verify if a host and port is up and running, and that the machine on which the script is running is not filtered by the firewall of the targeted host. Also, a CSV file can be given as input, if so the script will iterate over it and test each entry. In CSV mode, the script return a standard output formated also as a CSV.

The script is available for :
* Linux: Use the `connectivity-test.sh` script (prerequisite : having netcat installed).
* Windows/Powershell : Use the `connectivity.ps1` script.

In the examples below, the term `connectivity-test.[sh|ps1]` indicates that the feature works for `connectivity-test.sh` and `connectivity-test.ps1`.

**Usage** `./connect-test.[sh|ps1] [OPTIONS]`

## Parameters

The `connect-test.[sh|ps1]` script accept the following parameters:

| option | description |
| ------ | ----------- |
| `-h HOST` | Host or IP address to target, if option `-csv` is not used |
| `-p PORT` | Port to target, required if `-h` is used |
| `-udp` | Switch to UDP protocol, only if `-h` is used |
| `-msg MESSAGE` | Message to send during the connection attempt, default is *connectivity-test* |
| `-csv FILE` | CSV File containing a Gazelle TM Configuration table to iterate over |
| `-help` | Output a usage message and exit |

## Single test usage

To test a TCP endpoint:

```bash/powershell
$ ./connect-test.[sh|ps1] -h 127.0.0.1 -p 80 -msg "Connectivity test from my-system-name"

Connection to 127.0.0.1 80 port [tcp/http] succeeded!
```

To test an UDP endpoint:

```bash/powershell
$ ./connect-test.[sh|ps1] -h localhost -p 514 -udp -msg "Connectivity test from my-system-name"
```

 ## CSV test usage

The CSV file must have at least 12 the columns with the order and the meaning matching the table of Gazelle TM Configurations, that is `System`, `Table`, `Type`, `Actor`, `Host`, `IP`, `Port`, `isSecured`, `Details1`, `Details2`, `Details3` and `Approved`. The first raw (table header) will be ignored by the script.

*The CSV can be obtained from Gazelle TM, menu **Configuration** > **[MY COMPANY]: Systems configurations**. Filter according to the test objectives and click on the "[Export as Excel file]()" link. Once the `xls` file is downloaded, convert it as a CSV with comma (,) delimiters.*

### With bash script : 
To test a complete configurations list :

```bash
$ ./connect-test.sh -csv configurations.csv 

system,actor,type,host,ip,port,ip test result,host test result
GSS / IHE,ARR - Audit Record Repository,Syslog,gazelle-tools.ihe-europe.net,151.80.206.33,3001,PASSED,PASSED
PCCEL / KEREVAL,ARR - Audit Record Repository,Syslog,localhost,127.0.0.1,3001,FAILED,FAILED
PCCEL / KEREVAL,PDS - Patient Demographic Supplier,Webservice,pccel.kereval.com,127.0.0.1,80,PASSED,FAILED
```

The output summarize the configuration tested in CSV format, the two last columns are the tests result. The first result is the test using the IP address, the second is the test using the hostname that implies a DNS resolution.

The output can also be saved in a file:

```bash
$ ./connect-test.sh -csv configurations.csv > connectivity-results.csv
```

### With powershell script : 
To test a complete configurations list :
```bash
$ ./connect-test.ps1 -csv configurations.csv 
```

The results are automatically saved in the file `.\configurations_results.csv`




## Tips

In CSV mode, the script is using the column details1 to determine if the port of the endpoint is using UDP. Also, it is using the `Configuration Type` to skip tests with initiator actors.

The linux script has been tested on Ubuntu. It should work straitforward on Debian or Mac environment.
The Powershell script has been tested on Windows 11 with a Powershell terminal version 1.16.10262.0.

## Future evolutions

The script is based on `nc` command for Linux and on `Test-NetConnection` for Powershell, it is possible to improve the script to offer test possibility through an enterprise proxy.
