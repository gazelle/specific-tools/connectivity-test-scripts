#!/bin/bash

# Copyright 2020 IHE International (http://www.ihe.net)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

function show_help() {
	echo "HELP:"
	echo "connect-test performs a telnet call to the given host and port"
	echo " -csv file (Gazelle Configuration file as CSV to iterate over)"
	echo " -h host (Host or IP address to target, if -f is not used)"
	echo " -p port (Port to target, required if -h is used)"
	echo " -udp (Switch to UDP protocol, only if -h is used)"
	echo " -msg message (Message to send during the telnet attempt, default is 'connectivity-test')"
	echo " -help (display this help)"
	echo "example : connect-test -csv configuration.csv -msg \"Connectivity test from my-system-name\""
	echo "example : connect-test -h 127.0.0.1 -p 80 -msg \"Connectivity test from my-system-name\""
}

function test_single() {
  if [[ $4 == "UDP" ]]; then
    local udp_timeout=1
    echo $3 | nc -v -u -w $udp_timeout $1 $2 2>&1
  else
    echo $3 | nc -v -t $1 $2 2>&1
  fi
}

function compareIpHostResults() {
  if [[ ($1 == "FAILED" && $2 == "PASSED") ]]; then
	echo "Connectivity tests for this system only worked using the hostname, which may be normal depending on the system configuration."
  fi
}


function test_csv() {
  SYSTEMIFS=$IFS
  IFS=,
	local index=0
	echo "system,actor,type,host,ip,port,host test result,ip test result,remark"
	while read sys table connection_type actor host_name ip port is_secured details1 details2 details3 approved action
	do
		if [[ $index -gt 0 && ! ($connection_type =~ .*Initiator.*) ]];
		then
			testHost=$(test_single $host_name $port $2 $details1)
			( [[ $testHost =~ .*Connected.* ]] || [[ $testHost =~ .*succeeded.* ]] ) && testHost="PASSED" || testHost="FAILED"
			testIP=$(test_single $ip $port $2 $details1)
			( [[ $testIP =~ .*Connected.* ]] || [[ $testIP =~ .*succeeded.* ]] ) && testIP="PASSED" || testIP="FAILED"
			remark=$(compareIpHostResults $testIP $testHost)
			echo "$sys,$actor,$connection_type,$host_name,$ip,$port,$testHost,$testIP,$remark"
		fi
		((index+=1))
	done < $1
	IFS=$SYSTEMIFS
}

csv=
h=
p=
msg="connectivity-test"
transport="TCP"

while [ "$1" != "" ]; do
	case $1 in
		"-help")
			show_help
			exit 0
			;;
	  "-udp")
	    transport="UDP"
	    ;;
		*)
			name1=$1
			name=${name1:1}
			shift
			value=$1
			eval "$name='$value'"
			;;
	esac
	shift
done

if [ "x$csv" != "x" ]; then
	test_csv $csv $msg
elif [ "x$h" != "x" ]; then
	if [ "x$p" != "x" ]; then
		test_single $h $p $msg $transport
	else
		echo "Please provide a port"
        	show_help
        	exit 1
	fi
else
	echo "Please provide a CSV file or a host"
        show_help
        exit 1
fi
